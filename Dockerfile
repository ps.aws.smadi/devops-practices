FROM openjdk:11
WORKDIR /app
ARG VAR1
ARG VAR2 
#CMD -Dserver.port=8090	
ENV VAR3=$VAR1
ENV VAR4=$VAR2
COPY ./target/*.jar /app
CMD java -jar  $VAR3 $VAR4


